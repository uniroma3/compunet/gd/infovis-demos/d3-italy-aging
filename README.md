# D3 Italy aging

A small project using D3.js to show how to use the enter, update, exit D3 commands and how to use D3 scales.

## Online demo

An online demo can be found here: https://uniroma3.gitlab.io/compunet/gd/infovis-demos/d3-italy-aging


## Credits

This project was first realized by Valentino Di Donato and then edited by Giordano Da Lozzo.

